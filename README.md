# Computadora

Este trabajo contiene los dos mapas conceptuales solicitados, ambos son basados en los videos dados en la plataforma de Moodle.

## Von-Neumann vs Hardvard
```plantuml
@startmindmap
* Arquitectura de \ncomputadora
    **_ se basan en
        *** Modelos
            ****_ los cuáles son
                ***** Descripción funcional de la \nimplementación de varias partes que \n componen una computadora
                    ******_ existen dos modelos clásicos
                        ******* Modelo de\nVonn Neumann
                            ******** Bloques funcionales que \nconforman una computadora.
                                ********* Siempre deben \nestar conectadas entre sí.
                            ******** No hay que modificar \nel hardware para ejecutar\nun programa.
                            ******** La CPU,\nnúcleo central de\nla computadora.
                                ********* Ejecuta operaciones\nbásicas y es el encargado\nde gestionar el funcionamiento\ndel resto de componentes.
                            ******** Memoria principal.
                                ********* Lugar donde se almacenan\nlos datos como instrucciones.
                            ******** Buses
                                ********* Conexiones entre\nlos distintos componentes\ndel sistema.
                            ******** Periféricos
                                ********* Entrada y salida\nde datos.
                        ******* Modelo de \nHardvard
                            ******** Utiliza 2 memorias\nconectadas con buses\nindependientes.
                                ********* Una memoria se encarga de\n las instrucciones.
                                ********* Otra memoria se \nencarga de la colección\ndedatos.
                            ******** Buses independientes
                                ********* Las memorias pueden \ncontener diferente información en la \nmisma ubicación de ambas memorias.    

@endmindmap
```

## Supercomputadoras en México

```plantuml
@startmindmap
* Supercomputadoras en México
    ** Historia
        *** En 1957, Nabor Carrillo viaja a EUA\n por el interés de las supercomputadoras.\nDicho interés era para poder realizar cálculos de ingeniería.
        *** En 1958, la UNAM adquiere\nla computadora IBM650.
            **** Se ofreció a usuarios externos para su uso ya que \nla renta y importación a México salía bastante caro.
        *** En 1991, la UNAM adquiere la Cray 423.\nDicha computadora sirvió durante 10 año hasta\nentrando los años 2000.
    ** IBM650
        *** Leía información\n a través de tarjetas perforadas\ny funcionaba con bulbos.
            **** Contaba con una\nmemoria de 2K.
    ** Cray Y-MP 432
         *** Primer supercomputadora en Latinoamérica.
         *** Equivalía a dos mil computadoras\n de aquella época.
    ** KanBalam
        ***_ se compone
            **** Velocidad pico de procesamiento\n de 7.113 teraflops.
            **** 3.02TB en memoria RAM.
            **** 160TB de espacio en disco duro.
            **** 1368 núcleos AMD Opteron.
    ** Miztli
        ***_ se compone
            **** 332 nodos de cómputo\nHP Proliant SL230 y SL250.
                *****_ el cuál
                    ****** Por cada nodo existen 2 procesadores Intel Xeon.
            **** 23TB en memoria RAM.
            **** Capacidad de almacenamiento es de\n750TB en disco duro.
        
    ** Xiuhcoatl
        *** Segunda supercomputadora \nen Latinoamérica.\nBasada en clúster en el Cinvestav.
        ***_ se compone
            **** Capacidad de\nprocesamiento de 313 teraflops.
            **** Procesadores de arquitectura x86\n en 213 nodos.
            **** 21TB de memoria RAM.
            **** 78TB de espacio en disco duro. 
@endmindmap
```